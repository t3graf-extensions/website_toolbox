require([
  'jquery',
  'TYPO3/CMS/Core/Ajax/AjaxRequest'
], function ($,AjaxRequest) {
  $('.more-details').click(function (event) {
    $('#modal-ajax').modal('show');
    $('.modal-content').html('');
    $('#spinner-div').show();
    const themeKey = $(this).data('extensionkey');
    const rootPage = $(this).data('root-page')
      new AjaxRequest(TYPO3.settings.ajaxUrls.theme_detailsfromtheme)
        .withQueryArguments({themeKey:themeKey,rootPage:rootPage})
        .get()
        .then(async function (response) {
          const resolved = await response.resolve();
            $('.modal-content').html(resolved.html);
          $('#modalclose').click(function (event) {
            $('#modal-ajax').modal('hide');
          });
        });
  });
});
