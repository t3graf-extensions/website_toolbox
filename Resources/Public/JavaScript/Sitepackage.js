require([
  'jquery',
  'TYPO3/CMS/Core/Ajax/AjaxRequest',
], function ($,AjaxRequest) {
  $('.ext-overview').click(function (event) {
    $('#modal-ajax').modal('show');
    $('.modal-content').html('');
    $('#spinner-div').show();
    const extkey = $(this).data('extensionkey');
      new AjaxRequest(TYPO3.settings.ajaxUrls.sitepackage_detailsfromextension)
        .withQueryArguments({extensionKey:extkey})
        .get()
        .then(async function (response) {
          const resolved = await response.resolve();
            $('.modal-content').html(resolved.html);
          $('#modalclose').click(function (event) {
            $('#modal-ajax').modal('hide');
          });
        });
  });
});
