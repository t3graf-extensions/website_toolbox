<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Website Toolbox',
    'description' => 'Versatile, universal and very flexibly customizable website toolbox. It offers a tool for choosing themes (templates) and allows automatic integration of a project sitepackage. It is modularly expandable. See the documentation for more information.',
    'category' => 'module',
    'author' => 'T3graf Development-Team',
    'author_email' => 'development@t3graf-media.de',
    'state' => 'beta',
    'clearCacheOnLoad' => 0,
    'version' => '2.0.0',
    'constraints' => [
        'depends' => [
            'php' => '8.0.0 - 8.9.99',
            'typo3' => '11.5.0-11.5.99',
            'just_maintain' => '1.2.0-1.2.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
