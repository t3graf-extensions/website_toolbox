<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') or die();

(static function () {
    // Make the extension configuration accessible
    $extensionConfiguration = GeneralUtility::makeInstance(
        ExtensionConfiguration::class
    );
    $websiteToolboxConfiguration = $extensionConfiguration->get('website_toolbox');

    ExtensionUtility::registerModule(
        'WebsiteToolbox',
        'websiteTools',
        '',
        'after:web',
        [
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:website_toolbox/Resources/Public/Icons/modulegroup-website-toolbox.svg',
            'labels' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_mod_websitetoolbox.xlf',

        ]
    );

    if ((bool) $websiteToolboxConfiguration['enableThemes']) {
        ExtensionUtility::registerModule(
            'WebsiteToolbox',
            'websiteTools',
            'themesTool',
            '',
            [
                \T3graf\WebsiteToolbox\Controller\ThemeToolController::class => 'listThemes, activateTheme, themeDetails',

            ],
            [
                'access' => 'user,group',
                'icon' => 'EXT:website_toolbox/Resources/Public/Icons/module-theme.svg',
                'labels' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_theme.xlf',
                'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement',
                'path' => '/module/websitetools/themes/'
            ]
        );
    }
    if ((bool) $websiteToolboxConfiguration['enableSitepackage']) {
        ExtensionUtility::registerModule(
            'WebsiteToolbox',
            'websiteTools',
            'sitepackageTool',
            '',
            [
                \T3graf\WebsiteToolbox\Controller\SitepackageToolController::class => 'showSitepackageInfo, detailsFromExtension',

            ],
            [
                'access' => 'admin,systemMaintainer',
                'icon' => 'EXT:website_toolbox/Resources/Public/Icons/module-sitepackage.svg',
                'labels' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_sitepackage.xlf',
                'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement',
                'path' => '/module/websitetools/sitepackage/'
            ]
        );
    }
    // PageTS

    // Disable sys_template functions
    if ((bool) $websiteToolboxConfiguration['enableSitepackage']) {
        ExtensionManagementUtility::addPageTSConfig('
    mod.web_list.deniedNewTables := addToList(sys_template)
    mod.web_ts.menu.function.TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateConstantEditorModuleFunctionController = 0
    mod.web_ts.menu.function.TYPO3\CMS\Tstemplate\Controller\TypoScriptTemplateInformationModuleFunctionController = 0
    TCEFORM.pages.TSconfig.disabled=1
');
    }

    // Add own stylesheet(s) to TYPO3 backend

    $GLOBALS['TBE_STYLES']['skins']['website_toolbox'] = [];
    $GLOBALS['TBE_STYLES']['skins']['website_toolbox']['stylesheetDirectories']['css'] = 'EXT:website_toolbox/Resources/Public/Css/Backend';

    ExtensionManagementUtility::addLLrefForTCAdescr('tx_website_configuration', 'EXT:website_toolbox/Resources/Private/Language/locallang_csh_tx_website_configuration.xlf');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_website_configuration');
})();
