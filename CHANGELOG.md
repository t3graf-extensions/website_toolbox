# 2.0.0

## TASK

- 8edb560 [TASK] change some class names
- 0485643 [TASK] add the site path to the modules and clean up some code
- b05d2f8 [TASK] change extension status to beta

## BUGFIX

- d46bb2c [BUGFIX] fix array key warning if no site configuration exists

## Contributors

- Mike Tölle

# 1.1.2

## BUGFIX

- faa198e [BUGFIX] fix array warning in SitepackageController

## Contributors

- Mike Tölle

# 1.1.1

## FEATURE

- 1b3215f [FEATURE] add some more information to SitepackageOverview

## Contributors

- Mike Tölle

# 1.1.0

## FEATURE

- 6d56347 [FEATURE] Use the new folder structure for TYPO3 v11 and v12 if necessary

## TASK

- 49c5caf [TASK] add required PHP version

## BUGFIX

- 3d18f81 [BUGFIX] fix path problems between new and old folder structure

## Contributors

- Mike Tölle

# 1.0.2

## BUGFIX

- 02ee840 [BUGFIX] fix declaration of ResponseInterface

## Contributors

- Mike Tölle

# 1.0.1

## FEATURE

- 11f610f [FEATURE] add field sitePackage to site configuration

## TASK

- 26e2351 [TASK] remove some jobs form .gitlab-ci.yml

## Contributors

- Mike Tölle

# 1.0.0

## FEATURE

- 419513e [FEATURE] add theme detail modal.

## TASK

- 95850d4 [TASK] run code checks
- b00b05b [TASK] add upload to ter job to .gitlab-ci.yml
- 75de98b [TASK] change some functions in themeController
- 4f5a268 [TASK] update README.md
- 08f1e7d [TASK] add some files and folders to ignore.
- 4d43495 [TASK] make sitepackage module translatable.
- cdd788c [TASK] add some files and folders to ignore.
- fd49b77 [TASK] remove javaScript and css checks.
- e893103 [TASK] add javaScript and css checks.

## Contributors

- Mike Tölle

# 0.0.3

# 0.0.2

# 0.0.1

## FEATURE

- 41f0883 [FEATURE] add ajax modals to sitepackage information
- 26a124c [FEATURE] add toolbar items for active theme and active sitepackage
- 4d76b84 [FEATURE] add new TCA renderType imagePreview
- 4424e6f [FEATURE] add TypesBuilder for TCA configuration.
- 1a5c5c6 [FEATURE] add TypesBuilder for TCA configuration
- 64e8a27 [FEATURE] add preview button in buttonbar
- 3a4b671 [FEATURE] add Configuration module

## TASK

- 0163e3a [TASK] add automatic release create.
- 75c2005 [TASK] change .gitlab-ci.yml.
- 1738e7c [TASK] run code checks and fix them.
- 7549adb [TASK] add Continuous Integration for gitlab.
- 38de870 [TASK] check code for compliance with coding guidelines.
- 395fe25 [TASK] change required php version to 8.0
- 94dea4e [TASK] add web environment variables to .ddev/config.yaml
- 56a8df3 [TASK] build a new ent environment
- 4362005 [TASK] change css for previewImage
- 990765a [TASK] add description r imagePreview
- 4642342 [TASK] change php-cs-fixer settings and run cgl
- e0e8c19 [TASK] change translation of website configuration
- 3b7284d [TASK] ad website tab with customer info's
- 95c040c [TASK] build TCA with TypesBuilder
- f597c94 [TASK] fix cgl
- 484059e [TASK] make some changes
- daaf0d5 [TASK] set prefer-version to stable
- 3293e75 [TASK] add displaycondition and isThemeactive check
- 0ec3f7f [TASK] change PHP version to >=7.4
- b776437 [TASK] remove Agency Tools module and change websiteconf path
- 9186748 [TASK] run php-cs-fixer
- 08e3f24 [TASK] implement import of theme TypoScript
- f11bc6a [TASK] implement import of theme TypoScript
- c5b54fa [TASK] clean code
- 4855ccb [TASK] change info's in README.md
- c6d84d3 [TASK] update php version to 8.0
- 7320fb1 [TASK] add some info's in README.md
- bb9ae2e [TASK] change some badges in README.md
- acbaeaa [TASK] make some project specific changes.

## BUGFIX

- 6d722ae [BUGFIX] fix typo in release-job.yml.
- 741d1fa [BUGFIX] change settings for phpcs.
- 0233605 [BUGFIX] add requirements for ESlinter.
- db5e00d [BUGFIX] add composer requirements for CI/CD.
- aeaabd9 [BUGFIX] add composer requirements for CI/CD.
- 351d196 [BUGFIX] remove deprecated php version checks.
- e00babc [BUGFIX] fix types build error.
- 39dc7f6 [BUGFIX] fix some ajax call errors and php 8 errors
- 77c67a1 [BUGFIX] fix problems remove property in constants files
- cf2a0be [BUGFIX] fix persist problems for constant files
- c53497e [BUGFIX] fix configuration module entry
- 7af9076 [BUGFIX] add EXT: just-maintain
- 3aa23b6 [BUGFIX] make it compatible with php >= 7.4

## MISC

- 2c3b3a9 Initial commit

## Contributors

- Mike Tölle

