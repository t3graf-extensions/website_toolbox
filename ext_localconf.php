<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die();

(static function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
        '@import "EXT:website_toolbox/Configuration/TypoScript/setup.typoscript"'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants(
        '@import "EXT:website_toolbox/Configuration/TypoScript/constants.typoscript"'
    );
    // Register autoloading for TypoScript and theme

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing']['website_toolbox'] = \T3graf\WebsiteToolbox\Hooks\WebsiteTSLoader::class . '->includeWebsiteTypoScript';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1525590067] = [
        'nodeName' => 'imagePreview',
        'priority' => 30,
        'class' => \T3graf\WebsiteToolbox\Form\Element\ImagePreview::class,
    ];

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'systeminformation-activetheme',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:website_toolbox/Resources/Public/Icons/SystemInformation/activetheme.svg']
    );
    $iconRegistry->registerIcon(
        'systeminformation-activesitepackage',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:website_toolbox/Resources/Public/Icons/SystemInformation/activesitepackage.svg']
    );
})();
