<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

$extensionConfiguration = GeneralUtility::makeInstance(
    ExtensionConfiguration::class
);
$websiteToolboxConfiguration = $extensionConfiguration->get('website_toolbox');
if ((bool)$websiteToolboxConfiguration['enableSitepackage']) {
    $newColumns = [
        'sitePackage' => [
            'label' => 'Ext key of sitepackage',
            'config' => [
                'type' => 'input',
            ],
        ],
    ];

    $GLOBALS['SiteConfiguration']['site']['columns'] = array_merge_recursive(
        $GLOBALS['SiteConfiguration']['site']['columns'],
        $newColumns
    );

    $GLOBALS['SiteConfiguration']['site']['types']['0']['showitem'] .= ',--div--;Sitepackage,sitePackage';
}
