<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'sitepackage_detailsfromextension' => [
        'path' => '/sitepackage/detailsfromextension',
        'target' => \T3graf\WebsiteToolbox\Controller\SitepackageToolController::class . '::detailsFromExtensionAction',
    ],
    'theme_detailsfromtheme' => [
        'path' => '/theme/detailsfromtheme',
        'target' => \T3graf\WebsiteToolbox\Controller\ThemeToolController::class . '::detailsFromThemeAction',
    ],
];
