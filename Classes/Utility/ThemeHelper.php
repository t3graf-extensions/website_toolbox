<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Utility;

use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper to render a dynamic selection of available site extensions
 * and to fetch a theme for certain page
 */
class ThemeHelper
{
    /**
     * @var PackageManager
     */
    protected $siteHelper;

    /**
     * @var SiteFinder
     */
    protected $siteFinder;

    /**
     * @var PageRepository
     */
    protected PageRepository $pageRepository;
    /**
     * @var string|null
     */
    protected string|null $storagePath = '';

    public array $rootline = [];

    public function __construct(PackageManager $siteHelper, SiteFinder $siteFinder, PageRepository $pageRepository)
    {
        $this->siteHelper = $siteHelper;
        $this->siteFinder = $siteFinder;
        $this->pageRepository = $pageRepository;
    }

    /**
     * Reads the stored configuration (i.e. the extension model etc.).
     *
     * @param  string      $extensionKey
     * @param  string|null $storagePath
     * @return array|null
     */
    public function getThemeConfiguration(string $extensionKey, ?string $storagePath = null): ?array
    {
        $themeConfigurationYaml = self::getThemeYaml($extensionKey, $storagePath);
        return $themeConfigurationYaml ?? null;
    }

    public static function getThemeYaml(string $extensionKey, ?string $storagePath = null): ?array
    {
        $yaml = GeneralUtility::makeInstance(YamlFileLoader::class);
        $yamlFile = $storagePath . '/' . $extensionKey . '/' . 'theme.yaml';

        if (file_exists($yamlFile)) {
            return $yaml->load($yamlFile);
        }

        return null;
    }

    /**
     * Reads the stored configuration (i.e. the extension model etc.).
     *
     * @param  string      $extensionKey
     * @param  string|null $storagePath
     * @return string|null
     */
    public static function getThemeTitle(string $extensionKey, ?string $storagePath = null): ?string
    {
        if (!isset($storagePath)) {
            $storagePath = Environment::getExtensionsPath();
        }
        $themeConfigurationYaml = self::getThemeYaml($extensionKey, $storagePath);
        if (isset($themeConfigurationYaml)) {
            return $themeConfigurationYaml['title'];
        }
        return null;
    }

    /**
     * Reads the stored configuration (i.e. the extension model etc.).
     *
     * @param  string      $extensionKey
     * @param  string|null $storagePath
     * @return string|null
     */
    public static function getThemeVersion(string $extensionKey, ?string $storagePath = null): ?string
    {
        if (!isset($storagePath)) {
            $storagePath = Environment::getExtensionsPath();
        }
        $themeConfigurationYaml = self::getThemeYaml($extensionKey, $storagePath);
        if (isset($themeConfigurationYaml)) {
            return $themeConfigurationYaml['version'];
        }
        return null;
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public static function getActiveTheme(): ?string
    {
        $activeTheme = 'No theme activated';
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $allSites = $siteFinder->getAllSites();
        if (array_key_first($allSites) !== null) {
            $firstSite = $allSites[array_key_first($allSites)];
            $rootPageId = $firstSite->getRootPageId();
            $site = $siteFinder->getSiteByRootPageId($rootPageId);
            $configuration = $site->getConfiguration();
        }
        if (isset($configuration['activeTheme'])) {
            $activeTheme = (string)$configuration['activeTheme'];
        }
        return $activeTheme;
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public static function isThemeActive($theme): ?bool
    {
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $allSites = $siteFinder->getAllSites();
        if (array_key_first($allSites) !== null) {
            $firstSite = $allSites[array_key_first($allSites)];
            $rootPageId = $firstSite->getRootPageId();
            $site = $siteFinder->getSiteByRootPageId($rootPageId);
            $configuration = $site->getConfiguration();
        }
        if (isset($configuration['activeTheme'])) {
            $activeTheme = (string)$configuration['activeTheme'];
        }
        return $activeTheme === $theme;
    }
}
