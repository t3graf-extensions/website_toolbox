<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Utility;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Core\Package\PackageInterface;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;

/**
 * Helper to render a dynamic selection of available site extensions
 * and to fetch a theme for certain page
 */
class SiteHelper
{
    /**
     * @var PackageManager
     */
    protected $siteHelper;

    /**
     * @var ThemeHelper
     */
    protected ThemeHelper $themeHelper;

    /**
     * @var SiteFinder
     */
    protected $siteFinder;

    /**
     * @var PageRepository
     */
    protected PageRepository $pageRepository;

    //public mixed $rootPageId = 0;
    public mixed $rootPageId;
    public array $rootline = [];

    public function __construct(PackageManager $siteHelper, ThemeHelper $themeHelper, SiteFinder $siteFinder, PageRepository $pageRepository)
    {
        $this->siteHelper = $siteHelper;
        $this->themeHelper = $themeHelper;
        $this->siteFinder = $siteFinder;
        $this->pageRepository = $pageRepository;
    }

    public function getRootLine(): array
    {
        //$this->id = (int)GeneralUtility::_GP('id');
        /**
 * @phpstan-ignore-next-line
*/
        $this->id = $this->getPageUid();
        //krexx($this->id);
        if (isset($this->id)) {
            $rootlineUtility = GeneralUtility::makeInstance(RootlineUtility::class, $this->id);
            $this->rootline = $rootlineUtility->get();
        } else {
            $this->rootline[0]['uid'] = -1;
        }
        //krexx($this->rootline);

        return $this->rootline;
    }

    public function getPageUid(): int
    {
        //$pageUid = (GeneralUtility::_GP('id') > 0) ? GeneralUtility::_GP('id') : (int)$GLOBALS['_GET']['id'];
        /**
 * @phpstan-ignore-next-line
*/
        if (GeneralUtility::_GP('id') || isset($GLOBALS['_GET']['id'])) {
            $pageUid = (GeneralUtility::_GP('id') > 0) ? GeneralUtility::_GP('id') : (int)$GLOBALS['_GET']['id'];
        } else {
            $pageUid = 0;
        }
        return (int)$pageUid;
    }

    public function getRootPageId(): ?int
    {
        $rootline = $this->getRootLine();
        if ($rootline !== []) {
            $rootPageId = $rootline[0]['uid'];
        } else {
            $rootPageId = 0;
        }
        return $rootPageId;
    }

    public function getSiteThemePackage(int $pageId): ?PackageInterface
    {
        try {
            $site = $this->siteFinder->getSiteByRootPageId($pageId);
            $configuration = $site->getConfiguration();
            if (!isset($configuration['activeTheme'])) {
                return null;
            }
            $themeKey = (string)$configuration['activeTheme'];
            return $this->siteHelper->getPackage($themeKey);
        } catch (SiteNotFoundException $e) {
            return null;
        } catch (UnknownPackageException $e) {
            return null;
        }
    }

    public function getSitePackage(int $pageId): ?PackageInterface
    {
        try {
            $site = $this->siteFinder->getSiteByRootPageId($pageId);
            $configuration = $site->getConfiguration();

            if (!isset($configuration['sitePackage'])) {
                return null;
            }
            $sitePackage = (string)$configuration['sitePackage'];
            return $this->siteHelper->getPackage($sitePackage);
        } catch (SiteNotFoundException $e) {
            return null;
        } catch (UnknownPackageException $e) {
            return null;
        }
    }

    /**
     * @throws SiteNotFoundException
     */
    public function getCompleteSiteConfiguration(int $pageId): Site
    {
        return $this->siteFinder->getSiteByRootPageId($pageId);
    }
    /**
     * @return string[]
     */

    /**
     * @return string[]
     * @throws SiteNotFoundException
     */
    public function findAllThemeExtensions(): ?array
    {
        $result = null;
        $extensions = [];
        //check for new folder structure in TYPO3 v11 and v12
        $storagePath = Environment::getExtensionsPath();
        if (is_dir($storagePath)) {
            $allThemes = $this->findAllThemesInDirectory($storagePath);
        }

        // find theme.yaml in composer mode
        if (!is_dir($storagePath) && Environment::isComposerMode()) {
            $allThemes = $this->findAllThemesInVendor();
        }

        if ($allThemes) {
            $result = array_merge($extensions, $allThemes);
        }
        /**
         * @phpstan-ignore-next-line
         */
        return $result;
    }
    /**
     * @throws SiteNotFoundException
     */
    protected function findAllThemesInDirectory(string $storagePath): ?array
    {
        $result = [];
        $themeExtKey = '';
        $extensionDirectoryHandle = opendir($storagePath);
        /**
 * @phpstan-ignore-next-line
*/
        while (false !== ($singleExtensionDirectory = readdir($extensionDirectoryHandle))) {
            if ($singleExtensionDirectory[0] === '.') {
                continue;
            }
            $themeConfiguration = $this->themeHelper->getThemeConfiguration($singleExtensionDirectory, $storagePath);
            if (isset($themeConfiguration['themeExtKey'])) {
                $themeExtKey = $themeConfiguration['themeExtKey'];
            }
            $is_installed = ExtensionManagementUtility::isLoaded($themeExtKey);
            $isActive = false;
            if ($themeExtKey === $this->getActiveThemeKey($this->getRootPageId())) {
                $isActive = true;
            }

            if ($themeConfiguration !== null) {
                $result[$themeExtKey] ['isInstalled'] = $is_installed;
                $result[$themeExtKey] ['isActive'] = $isActive;
                $result[$themeExtKey] ['extKey'] = $themeExtKey;
                $result[$themeExtKey] = array_merge(
                    $result[$themeExtKey],
                    $themeConfiguration
                );
            }
        }
        /**
 * @phpstan-ignore-next-line
*/
        closedir($extensionDirectoryHandle);

        $sortresult = $result;
        /**
 * @phpstan-ignore-next-line
*/
        if (isset($result) && $this->getActiveThemeKey($this->getRootPageId()) !== null) {
            $sortresult = self::sortActiveThemeFirst($result, $this->getActiveThemeKey($this->getRootPageId()));
        }
        return $sortresult;
    }

    /**
     * @throws SiteNotFoundException
     */
    protected function findAllThemesInVendor(): ?array
    {
        $result = [];
        $themeExtKey = '';
        $composerFile = Environment::getProjectPath() . '/';
        $strJsonFileContents = file_get_contents($composerFile . 'composer.json');
        $jsonArray = json_decode($strJsonFileContents, true);
        if (isset($jsonArray['config']['vendor-dir'])) {
            $vendorStoragePath = $composerFile . $jsonArray['config']['vendor-dir'];
        } else {
            $vendorStoragePath = Environment::getProjectPath() . '/vendor';
        }

        $vendorDirectoryHandle = opendir($vendorStoragePath);
        /**
         * @phpstan-ignore-next-line
         */
        // Get single vendors
        while (false !== ($vendorExtensionDirectory = readdir($vendorDirectoryHandle))) {
            if ($vendorExtensionDirectory[0] === '.' || $vendorExtensionDirectory === 'autoload.php') {
                continue;
            }
            $extensionStoragePath = $vendorStoragePath . '/' . $vendorExtensionDirectory;

            $extensionDirectoryHandle = opendir($extensionStoragePath);
            while (false !== ($singleExtensionDirectory = readdir($extensionDirectoryHandle))) {
                if ($singleExtensionDirectory[0] === '.') {
                    continue;
                }

                $themeConfiguration = $this->themeHelper->getThemeConfiguration($singleExtensionDirectory, $extensionStoragePath);

                if (isset($themeConfiguration['themeExtKey'])) {
                    $themeExtKey = $themeConfiguration['themeExtKey'];
                }
                $is_installed = ExtensionManagementUtility::isLoaded($themeExtKey);
                $isActive = false;
                if ($themeExtKey === $this->getActiveThemeKey($this->getRootPageId())) {
                    $isActive = true;
                }

                if ($themeConfiguration !== null) {
                    $result[$themeExtKey] ['isInstalled'] = $is_installed;
                    $result[$themeExtKey] ['isActive'] = $isActive;
                    $result[$themeExtKey] ['extKey'] = $themeExtKey;
                    $result[$themeExtKey] = array_merge(
                        $result[$themeExtKey],
                        $themeConfiguration
                    );
                }
            }
        }

        /**
         * @phpstan-ignore-next-line
         */
        closedir($extensionDirectoryHandle);

        $sortresult = $result;
        /**
         * @phpstan-ignore-next-line
         */
        if (isset($result) && $this->getActiveThemeKey($this->getRootPageId()) !== null) {
            $sortresult = self::sortActiveThemeFirst($result, $this->getActiveThemeKey($this->getRootPageId()));
        }
        return $sortresult;
    }

    /**
     * Sort the themes so that the active theme is first .
     *
     * @param  array $availableThemes
     * @return array|null
     */
    public static function sortActiveThemeFirst(array $availableThemes, ?string $activeTheme): ?array
    {
        $sortResult = [];
        if (array_key_exists($activeTheme, $availableThemes)) {
            $sortResult[$activeTheme] = $availableThemes[$activeTheme];
            unset($availableThemes[$activeTheme]);
        }

        return array_merge($sortResult, $availableThemes);
    }

    /**
     * @throws SiteNotFoundException
     */
    public function getActiveThemeKey(?int $rootPageId): ?string
    {
        if ($rootPageId > 0) {
            $completeSiteConf = $this->siteFinder->getSiteByRootPageId($rootPageId);
            $siteConf = $completeSiteConf->getConfiguration();
            if (!array_key_exists('activeTheme', $siteConf)) {
                $siteConf['activeTheme'] = null;
            }
            return $siteConf['activeTheme'];
        }
        return null;
    }

    public function getAllRootSites(): array
    {
        $pageRepository = $this->pageRepository;
        $sites = array_map(
            /**
             * @phpstan-ignore-next-line
             */
            static function (Site $site) use ($pageRepository) {
                $page = $pageRepository->getPage($site->getRootPageId());
                $title = trim($page['title'] ?? $site->getIdentifier());
                $title = $title === '' ? $page['subtitle'] : $title;
                if (!isset($page['uid'])) {
                    $page['uid'] = 0;
                }
                return [
                    'rootPage' => $page['uid'],
                    'rootPageTitle' => $title
                ];
            },
            GeneralUtility::makeInstance(SiteFinder::class)->getAllSites()
        );
        /**
 * @phpstan-ignore-next-line
*/
        return array_filter($sites, static fn (array $site): bool => $site !== []);
    }
}
