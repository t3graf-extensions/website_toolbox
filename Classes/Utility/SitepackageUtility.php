<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Utility;

use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper to render a dynamic selection of available site extensions
 * and to fetch a theme for certain page
 */
class SitepackageUtility
{
    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public static function getActiveSitepackage(): ?string
    {
        $activeSitepackage = 'No sitepackage available';
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $allSites = $siteFinder->getAllSites();
        if (array_key_first($allSites) !== null) {
            $firstSite = $allSites[array_key_first($allSites)];
            $rootPageId = $firstSite->getRootPageId();
            $site = $siteFinder->getSiteByRootPageId($rootPageId);
            $configuration = $site->getConfiguration();
        }
        if (isset($configuration['sitePackage'])) {
            $activeSitepackage = (string)$configuration['sitePackage'];
        }
        return $activeSitepackage;
    }

    // This method is used for get all pages of site

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public static function countPages(): int
    {
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('pages');
        /**
 * @phpstan-ignore-next-line
*/
        $totalPages = $queryBuilder
            ->count('uid')
            ->from('pages')
            ->execute()
            ->fetchOne(0);

        return $totalPages;
    }

    // This method is used for get all domains of site

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function countDomain(): int
    {
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
                ->getQueryBuilderForTable('sys_domain');
        /**
 * @phpstan-ignore-next-line
*/
        $totalDomain = $queryBuilder
            ->count('pid')
            ->from('sys_domain')
            ->groupBy('pid')
            ->execute()
            ->fetchOne(0);

        if ($totalDomain>0) {
            return $totalDomain;
        } else {
            return 1;
        }
    }

    // This method is used for get all system language of site

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public static function sysLang(): int
    {
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('sys_language');
        /**
 * @phpstan-ignore-next-line
*/
        $totalLang = $queryBuilder
            ->count('uid')
            ->from('sys_language')
            ->execute()
            ->fetchOne(0);

        return $totalLang+1;
    }
}
