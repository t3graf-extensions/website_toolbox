<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Backend\ToolbarItem;

use T3graf\WebsiteToolbox\Utility\SitepackageUtility;
use T3graf\WebsiteToolbox\Utility\ThemeHelper;
use TYPO3\CMS\Backend\Backend\Event\SystemInformationToolbarCollectorEvent;
use TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * VersionToolbarItem
 */
class VersionToolbarItem
{
    public function __invoke(SystemInformationToolbarCollectorEvent $event): void
    {
        $this->addVersionInformation($event->getToolbarItem());
    }
    /**
     * Called by the system information toolbar signal/slot dispatch.
     *
     * @param SystemInformationToolbarItem $systemInformation
     */
    public function addVersionInformation(SystemInformationToolbarItem $systemInformation): void
    {
        // Make the extension configuration accessible
        $extensionConfiguration = GeneralUtility::makeInstance(
            ExtensionConfiguration::class
        );
        $websiteToolboxConfiguration = $extensionConfiguration->get('website_toolbox');
        // Set system information entry

        if ((bool) $websiteToolboxConfiguration['enableThemes']) {
            $activeThemeInfo = (string)ThemeHelper::getActiveTheme();
            $systemInformation->addSystemInformation(
                'Active Theme',
                htmlspecialchars($activeThemeInfo, ENT_QUOTES | ENT_HTML5),
                'systeminformation-activetheme'
            );
        }
        if ((bool) $websiteToolboxConfiguration['enableSitepackage']) {
            $activeSitepackage = (string)SitepackageUtility::getActiveSitepackage();
            $systemInformation->addSystemInformation(
                'Sitepackage',
                htmlspecialchars($activeSitepackage, ENT_QUOTES | ENT_HTML5),
                'systeminformation-activesitepackage'
            );
        }
    }
}
