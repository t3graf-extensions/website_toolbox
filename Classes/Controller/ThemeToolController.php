<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Controller;

/**
 * This file is part of the "T3 Theme Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Development-Team [T3graf media-agentur] <development@t3graf-media.de>, T3graf media-agentur UG
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use T3graf\WebsiteToolbox\Utility\SiteHelper;
use T3graf\WebsiteToolbox\Utility\ThemeHelper;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * ThemeController
 */
class ThemeToolController extends ActionController
{
    /**
     * @var ModuleTemplateFactory
     */
    protected ModuleTemplateFactory $moduleTemplateFactory;

    /**
     * @var ThemeHelper
     */
    protected ThemeHelper $themeHelper;

    /**
     * @var SiteHelper
     */
    protected SiteHelper $siteHelper;

    /**
     * @var SiteConfiguration
     */
    protected SiteConfiguration $siteConfiguration;

    public function __construct(SiteHelper $siteHelper, ThemeHelper $themeHelper, ModuleTemplateFactory $moduleTemplateFactory, SiteConfiguration $siteConfiguration)
    {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->themeHelper = $themeHelper;
        $this->siteHelper = $siteHelper;
        $this->siteConfiguration = $siteConfiguration;
    }

    public function initializeAction() : void
    {
        parent::initializeAction();
        //$this->id = (int)GeneralUtility::_GP('id');
        //$this->id = $this->request->getAttribute('site');
        // krexx(GeneralUtility::_GP('id'));
        //  krexx($GLOBALS);
        /*  if (GeneralUtility::_GP('id') || $GLOBALS['_GET']['id']){
              $this->pageUid = (GeneralUtility::_GP('id') > 0) ? GeneralUtility::_GP('id') : (int)$GLOBALS['_GET']['id'];
          } else {
              $this->pageUid = 0;
          }
        */
        /**
 * @phpstan-ignore-next-line
*/
        $this->pageUid = $this->siteHelper->getPageUid();
    }

    /**
     * action listThemes
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     * @throws \TYPO3\CMS\Core\Routing\UnableToLinkToPageException
     */
    public function listThemesAction(): ResponseInterface
    {
        $rootPages = $this->siteHelper->getAllRootSites();
        $rootPageId = $this->siteHelper->getRootPageId();
        $rootline = $this->siteHelper->getRootLine();
        $previewUrl = '';
        if ($rootPageId > 0) {
            $previewUrl = \TYPO3\CMS\Backend\Utility\BackendUtility::getPreviewUrl($rootPageId);
        }

        // LIST all installed themes
        $availablethemes = $this->siteHelper->findAllThemeExtensions();
        if (!isset($availablethemes)) {
            $this->addFlashMessage(
                /**
 * @phpstan-ignore-next-line
*/
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:theme.noTheme.description', 'website_toolbox'),
                /**
                * @phpstan-ignore-next-line
                */
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:theme.noTheme.title', 'website_toolbox'),
                AbstractMessage::ERROR
            );
        } elseif ($this->siteHelper->getActiveThemeKey($rootPageId) === null && $rootPageId > 0) {
            $this->addFlashMessage(
                /**
 * @phpstan-ignore-next-line
*/
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:theme.noActiveTheme.description', 'website_toolbox'),
                /**
                * @phpstan-ignore-next-line
                */
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:theme.noActiveTheme.title', 'website_toolbox'),
                AbstractMessage::WARNING
            );
        }

        if (!isset($rootline[0])) {
            $rootline[0] = null;
            $this->addFlashMessage(
                /**
                 * @phpstan-ignore-next-line
                 */
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:theme.noTheme.description', 'website_toolbox'),
                /**
                 * @phpstan-ignore-next-line
                 */
                'No Root page selected',
                AbstractMessage::WARNING
            );
        }
        $this->view->assignMultiple(
            [
            'rootPage' => $rootPageId,
            'rootPages' => $rootPages,
            'currentPage' =>  $rootline[0],
            'activeTheme' => $this->siteHelper->getActiveThemeKey($rootPageId),
            'availableThemes' => $availablethemes,
            'previewUrl' => $previewUrl,
            ]
        );
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile('EXT:website_toolbox/Resources/Public/Css/Theme.css', 'stylesheet', 'all', '', true);

        return $this->htmlResponse();
    }

    /**
     * action activateTheme
     *
     * @throws \TYPO3\CMS\Core\Configuration\Exception\SiteConfigurationWriteException
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function activateThemeAction(): void
    {
        $rootPageId = (int)$GLOBALS['_GET']['id'];
        $themeToActivated = (string)$GLOBALS['_GET']['themeKey'];
        $completeSiteConf = $this->siteHelper->getCompleteSiteConfiguration($rootPageId);
        $siteConf = $completeSiteConf->getConfiguration();
        $siteConf['activeTheme'] = $themeToActivated;
        $this->siteConfiguration->write($completeSiteConf->getIdentifier(), $siteConf);
        $this->addFlashMessage(LocalizationUtility::translate('theme.success.themeActivated', 'website_toolbox') . ' ' . $themeToActivated, 'Success', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        $this->redirect('listThemes');
    }

    /**
     * Shows details of a specific extension
     *
     * @param  ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function detailsFromThemeAction(ServerRequestInterface $request): ResponseInterface
    {
        $theme = [];
        $instExt = [];
        $view = $this->initializeStandaloneView($request, 'ThemeTool/DetailsFromTheme.html');

        $themeKey = $request->getQueryParams()['themeKey'] ?? null;
        $rootPage = $request->getQueryParams()['rootPage'] ?? null;
        //krexx($extKey);
        if ($themeKey === null) {
            throw new \InvalidArgumentException('Please choose a extension key for the theme', 158058511);
        }
        //Get theme list
        $allThemes = $this->siteHelper->findAllThemeExtensions();
        foreach ($allThemes as $extensionKey => $instExt) {
            if ($instExt['extKey'] === $themeKey) {
                if ($instExt['extKey'] === $this->siteHelper->getActiveThemeKey((int) $rootPage)) {
                    $instExt['isActive'] = true;
                }
                $theme = $instExt;
            }
        }

        $view->assign('theme', $theme);
        $view->assign('rootPage', $rootPage);

        $modalContent = $view->render();

        return new JsonResponse(
            [
                'success' => true,
                'html' => $modalContent,
            ]
        );
    }

    /**
    * Helper method to initialize a standalone view instance.
    *
    * @param    ServerRequestInterface $request
    * @param    string                 $templatePath
    * @return   StandaloneView
    * @internal param string $template
    */
    protected function initializeStandaloneView(ServerRequestInterface $request, string $templatePath): StandaloneView
    {
        $viewRootPath = GeneralUtility::getFileAbsFileName('EXT:website_toolbox/Resources/Private/Backend/');
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->getRequest()->setControllerExtensionName('WebsiteToolbox');
        $view->setTemplatePathAndFilename($viewRootPath . 'Templates/' . $templatePath);
        $view->setLayoutRootPaths([$viewRootPath . 'Layouts/']);
        $view->setPartialRootPaths([$viewRootPath . 'Partials/']);
        $view->assignMultiple(
            [
                'controller' => $request->getQueryParams()['tx_websitetoolbox_websitetoolboxwebsitetools_websitetoolboxsitepackage']['controller'] ?? 'theme',
                'context' => $request->getQueryParams()['tx_websitetoolbox_websitetoolboxwebsitetools_websitetoolboxsitepackage']['context'] ?? '',
                'composerMode' => Environment::isComposerMode(),
                'currentTypo3Version' => (string)(new Typo3Version()),
            ]
        );

        return $view;
    }
}
