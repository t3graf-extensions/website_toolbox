<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use T3graf\WebsiteToolbox\Utility\SiteHelper;
use T3graf\WebsiteToolbox\Utility\SitepackageUtility;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extensionmanager\Domain\Repository\ExtensionRepository;
use TYPO3\CMS\Extensionmanager\Utility\ListUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * SitepackageController
 */
class SitepackageToolController extends ActionController
{
    /**
     * @var ModuleTemplateFactory
     */
    protected ModuleTemplateFactory $moduleTemplateFactory;

    /**
     * @var SiteHelper
     */
    protected SiteHelper $siteHelper;

    /**
     * @var ListUtility
     */
    protected ListUtility $listUtility;

    /**
     * @var ExtensionRepository
     */
    protected ExtensionRepository $extensionRepository;

    public function __construct(
        SiteHelper $siteHelper,
        ModuleTemplateFactory $moduleTemplateFactory,
        ListUtility $listUtility,
        ExtensionRepository $extensionRepository
    ) {
        $this->siteHelper = $siteHelper;
        $this->moduleTemplateFactory = $moduleTemplateFactory;
        $this->listUtility = $listUtility;
        $this->extensionRepository = $extensionRepository;
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function showSitepackageInfoAction(): ResponseInterface
    {
        $rootPages = $this->siteHelper->getAllRootSites();
        $rootPageId = $this->siteHelper->getRootPageId();
        $rootline = $this->siteHelper->getRootLine();
        /**
 * @phpstan-ignore-next-line
*/
        $sysDetail = $this->getSysDetail();
        $sitepackageDetail = $this->getSitepackageDetail();
        $previewUrl = '';
        if ($rootPageId > 0) {
            $previewUrl = \TYPO3\CMS\Backend\Utility\BackendUtility::getPreviewUrl($rootPageId);
        }

        /**
 * @phpstan-ignore-next-line
*/
        if ($this->siteHelper->getSitePackage($rootPageId) === null && $rootPageId > 0) {
            $this->addFlashMessage(
                /**
 * @phpstan-ignore-next-line
*/
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:sitePackage.noPackage.description', 'website_toolbox'),
                /**
                * @phpstan-ignore-next-line
                */
                LocalizationUtility::translate('LLL:EXT:website_toolbox/Resources/Private/Language/locallang.xlf:sitePackage.noPackage.title', 'website_toolbox'),
                AbstractMessage::ERROR
            );
        }
        $currentPage = $rootline[0] ?? 0;

        $sysDetail['rootPages'] = count($rootPages);
        $assignArray = $this->getAllExtensions();
        $assignArray['rootPage'] = $rootPageId;
        $assignArray['rootPages'] = $rootPages;
        $assignArray['currentPage'] = $currentPage;
        $assignArray['sitepackageDetail'] = $sitepackageDetail;
        $assignArray['previewUrl'] = $previewUrl;
        $assignArray['sysDetail'] = $sysDetail;

        $this->view->assignMultiple($assignArray);

        return $this->htmlResponse();
    }

    /**
     * Shows details of a specific extension
     *
     * @param  ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function detailsFromExtensionAction(ServerRequestInterface $request): ResponseInterface
    {
        $extension = '';
        $view = $this->initializeStandaloneView($request, 'SitepackageTool/DetailsFromExtension.html');

        $extKey = $request->getQueryParams()['extensionKey'] ?? null;
        //krexx($extKey);
        if ($extKey === null) {
            throw new \InvalidArgumentException('Please choose a extension key', 1580585107);
        }
        //Get extension list
        $allExtensions = $this->listUtility->getAvailableAndInstalledExtensionsWithAdditionalInformation();
        foreach ($allExtensions as $nsExt) {
            $newNsVersion = 0;
            //Filter all local extension for whole TER data start
            if (strtolower($nsExt['type']) === 'local' && $nsExt['key'] === $extKey) {
                $extension = $nsExt;
            }
        }
        //Filter all local extension for whole TER data end

        $sysDetail = $this->getSysDetail();

        $view->assign('sysDetail', $sysDetail);
        $view->assign('extension', $extension);

        $modalContent = $view->render();

        return new JsonResponse(
            [
                'success' => true,
                'html' => $modalContent,
            ]
        );
    }

    /**
     * This method is used of get sysimformation
     */
    public function getSysDetail(): array
    {
        $sysDetail = [];
        $sysDetail['phpversion'] = substr(PHP_VERSION, 0, 6);
        $sysDetail['sitename'] = $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'];
        $sysDetail['typo3version'] = VersionNumberUtility::getNumericTypo3Version();
        $sysDetail['totalPages'] = SitepackageUtility::countPages();
        $sysDetail['totalLang'] = SitepackageUtility::sysLang();
        $sysDetail['composerMode'] = Environment::isComposerMode();

        return $sysDetail;
    }

    /**
     * This method is used of get site package information
     *
     * @throws \JsonException
     */
    public function getSitepackageDetail(): array
    {
        $sitepackageDetail = [];
        $rootPageId = $this->siteHelper->getRootPageId();
        $sitePackage = $this->siteHelper->getSitePackage($rootPageId);

        if ($rootPageId > 0 && $sitePackage) {
            $sitePackageAuthors = $sitePackage->getValueFromComposerManifest('authors');
            $sitePackageSupport = $sitePackage->getValueFromComposerManifest('support');
            $authorsArray = json_decode(json_encode($sitePackageAuthors, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
            $supportArray = json_decode(json_encode($sitePackageSupport, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
            $sitePackageMetaData = $sitePackage->getPackageMetaData();
            $sitepackageDetail['description'] = $sitePackageMetaData->getDescription();
            $sitepackageDetail['version'] = $sitePackageMetaData->getVersion();
            $sitepackageDetail['author'] = $authorsArray[0]['name'] ?? '';
            $sitepackageDetail['authorEmail'] = $authorsArray[0]['email'] ?? '';
            $sitepackageDetail['supportEmail'] = $supportArray['email'] ?? '';
            $sitepackageDetail['supportIssues'] = $supportArray['issues'] ?? '';
        }
        return $sitepackageDetail;
    }

    /**
     * This method is used for  get detail list of local extension
     */
    public function getAllExtensions(): array
    {
        $i = 1;
        $activeSitepackage = '';
        $totalInstalled = 0;
        $totalNonInstalled = 0;
        $assignArray = [];
        $extensionlist = [];
        $overviewReport = [];
        $sitepackage = $this->siteHelper->getSitePackage($this->siteHelper->getRootPageId());
        if ($sitepackage) {
            $activeSitepackage = $sitepackage->getPackageKey();
        }

        //Get extension list
        $allExtensions = $this->listUtility->getAvailableAndInstalledExtensionsWithAdditionalInformation();

        foreach ($allExtensions as $extensionKey => $instExt) {
            //Filter all local extension for whole TER data start
            if (strtolower($instExt['type']) === 'local' && $instExt['key'] !== 'website_toolbox' && $instExt['key'] !== $activeSitepackage) {
                $newNsVersion = 0;
                $extArray = $this->extensionRepository->findByExtensionKeyOrderedByVersion($instExt['key']);

                //Fetch typo3 depency of extesion  start
                if (count($extArray) != 0) {
                    foreach ($extArray as $extension) {
                        foreach ($extension->getDependencies() as $dependency) {
                            if ($dependency->getIdentifier() === 'typo3') {
                                // Extract min TYPO3 CMS version (lowest)
                                $minVersion = $dependency->getLowestVersion();
                                // Extract max TYPO3 CMS version (higherst)
                                $maxVersion = $dependency->getHighestVersion();
                                $instExt['sitepackageExtensions'] = 1;
                            }
                        }
                    }
                } else {
                    $instExt['customExt'] = true;
                }
                //Fetch typo3 depency of extesion  end

                // Set overview Report start

                if ($extArray[0] && $instExt['newVersion'] !== '') {
                    $instExt['newVersion'] = $extArray[0]->getVersion();
                }

                if ($instExt['installed'] == 1) {
                    $totalInstalled++;
                } else {
                    $totalNonInstalled++;
                }
                //Count Total compatibility End

                // Set overview Report end
                $extensionlist[$i] = $instExt;
                $i++;
            }
            //Filter all local extension for whole TER data end
        }
        //Set overview array start
        $overviewReport['totalInstalled'] = $totalInstalled;
        $overviewReport['totalNonInstalled'] = $totalNonInstalled;
        //Set overview array end

        $assignArray['overviewReport'] = $overviewReport;
        $assignArray['extensionlist'] = $extensionlist;

        return $assignArray;
    }

    /**
     * Helper method to initialize a standalone view instance.
     *
     * @param    ServerRequestInterface $request
     * @param    string                 $templatePath
     * @return   StandaloneView
     * @internal param string $template
     */
    protected function initializeStandaloneView(ServerRequestInterface $request, string $templatePath): StandaloneView
    {
        $viewRootPath = GeneralUtility::getFileAbsFileName('EXT:website_toolbox/Resources/Private/Backend/');
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->getRequest()->setControllerExtensionName('WebsiteToolbox');
        $view->setTemplatePathAndFilename($viewRootPath . 'Templates/' . $templatePath);
        $view->setLayoutRootPaths([$viewRootPath . 'Layouts/']);
        $view->setPartialRootPaths([$viewRootPath . 'Partials/']);
        $view->assignMultiple(
            [
            'controller' => $request->getQueryParams()['tx_websitetoolbox_websitetoolboxwebsitetools_websitetoolboxsitepackage']['controller'] ?? 'sitepackage',
            'context' => $request->getQueryParams()['tx_websitetoolbox_websitetoolboxwebsitetools_websitetoolboxsitepackage']['context'] ?? '',
            'composerMode' => Environment::isComposerMode(),
            'currentTypo3Version' => (string)(new Typo3Version()),
            ]
        );

        return $view;
    }
}
