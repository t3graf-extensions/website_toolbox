<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Form\Element;

use TYPO3\CMS\Backend\Form\AbstractNode;
use TYPO3\CMS\Backend\Form\Element\InputTextElement;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class ImagePreview extends AbstractNode
{
    public function render()
    {
        //krexx($this->data);
        $resultArray = [
            'html' => '',
        ];
        // first get input element
        $inputField = GeneralUtility::makeInstance(InputTextElement::class, $this->nodeFactory, $this->data);
        $resultArray = $inputField->render();
        // krexx($this->data['fieldName']);

        // Load necessary JavaScript
        // $resultArray['requireJsModules'] = $this->loadJavascript();

        // Load necessary CSS
        $resultArray['stylesheetFiles'] = $this->loadCss();

        // add wizard content
        $resultArray['html'] .= $this->getBodyContent($this->data['databaseRow'][$this->data['fieldName']]);
        //krexx($resultArray);
        return $resultArray;
    }
    /**
     * Load the necessary javascript
     *
     * This will only be done when the referenced record is available
     *
     * @return array
     */
    protected function loadJavascript()
    {
        return [
            'snippetPreview' => [
                'TYPO3/CMS/CsSeo/FormEngine/Element/SnippetPreview' => 'function(SnippetPreview){SnippetPreview.initialize()}'
            ]
        ];
    }

    /**
     * Load the necessary css
     *
     * This will only be done when the referenced record is available
     *
     * @return array
     */
    protected function loadCss()
    {
        $stylesheetFiles = [];
        $cssFiles = [
            'ImagePreview.css'
        ];
        $baseUrl = 'EXT:website_toolbox/Resources/Public/Css/';
        // Load the wizards css
        foreach ($cssFiles as $cssFile) {
            $stylesheetFiles[] = $baseUrl . $cssFile;
        }

        return $stylesheetFiles;
    }

    /**
     * Generate the body content
     *
     * If there is an error, no reference to a record, a Flash Message will be
     * displayed
     *
     * @return string The body content
     */
    protected function getBodyContent(string $previewImage)
    {
        //krexx($previewImage);
        // template1
        /**
 * @var StandaloneView $wizardView
*/
        $wizardView = GeneralUtility::makeInstance(StandaloneView::class);
        $wizardView->setFormat('html');
        $wizardView->setLayoutRootPaths(
            [10 => 'EXT:website_toolbox/Resources/Private/Backend/Layouts/']
        );
        $wizardView->setTemplatePathAndFilename(
            'EXT:website_toolbox/Resources/Private/Backend/Templates/FormFields/ImagePreview.html'
        );

        $wizardView->assignMultiple(
            [
                'previewImage' => $previewImage,
            ]
        );

        return $wizardView->render();
    }

    /**
     * @return PageRenderer
     */
    protected function getPageRenderer(): PageRenderer
    {
        /**
 * @phpstan-ignore-next-line
*/
        if ($this->pageRenderer === null) {
            $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        }

        return $this->pageRenderer;
    }

    /**
     * Returns an instance of LanguageService
     *
     * @return \TYPO3\CMS\Core\Localization\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}
