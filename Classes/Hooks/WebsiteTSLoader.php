<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\Hooks;

use T3graf\WebsiteToolbox\Utility\SiteHelper;
use T3graf\WebsiteToolbox\Utility\ThemeHelper;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hooks into the process of building TypoScript templates
 * only works with TYPO3 v8.6.0 natively, otherwise the XCLASS kicks in
 */
class WebsiteTSLoader
{
    /**
     * @var SiteHelper
     */
    protected SiteHelper $siteHelper;

    /**
     * @var ThemeHelper
     */
    protected ThemeHelper $themeHelper;

    /**
     * @var ServerRequest
     */
    protected ServerRequest $request;

    public function __construct(SiteHelper $siteHelper, ThemeHelper $themeHelper, ServerRequest $request)
    {
        $this->siteHelper = $siteHelper;
        $this->themeHelper = $themeHelper;
        $this->request = $request;
    }

    /**
     *  $hookParameters = [
     *      'extensionStaticsProcessed' => &$this->extensionStaticsProcessed,
     *      'isDefaultTypoScriptAdded'  => &$this->isDefaultTypoScriptAdded,
     *      'absoluteRootLine' => &$this->absoluteRootLine,
     *      'rootLine'         => &$this->rootLine,
     *      'startTemplateUid' => $start_template_uid,
     *  ];
     *
     * @param  array           $hookParameters
     * @param  TemplateService $templateService
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function includeWebsiteTypoScript(&$hookParameters, TemplateService $templateService): void
    {
        // Make the extension configuration accessible
        $extensionConfiguration = GeneralUtility::makeInstance(
            ExtensionConfiguration::class
        );
        $websiteToolboxConfiguration = $extensionConfiguration->get('website_toolbox');

        if ((bool)$websiteToolboxConfiguration['enableSitepackage']) {
            $themeSetupFile = '';
            $themeConstantsFile = '';
            $themePackage = '';
            $pageWebsiteConstantsFile = [];

            // let's copy the rootline value, as $templateService->processTemplate() might reset it
            $rootLine = $hookParameters['rootLine'];
            if (!is_array($rootLine) || $rootLine === []) {
                return;
            }

            foreach ($rootLine as $level => $pageRecord) {
                $filename = Environment::getConfigPath() . '/easyconf/Configuration/TypoScript/EasyconfConstantsP' . $pageRecord['uid'] . '.typoscript';
                if (file_exists($filename)) {
                    $pageWebsiteConstantsFile[$pageRecord['uid']] = "@import '" . $filename . "'\n";
                }
            }
            foreach ($rootLine as $level => $pageRecord) {
                $sitepackage = $this->siteHelper->getSitePackage((int)$pageRecord['uid']);
                if ($pageRecord['is_siteroot'] === 1) {
                    $themePackage = $this->siteHelper->getActiveThemeKey((int)$pageRecord['uid']);
                }

                if ($sitepackage !== null) {
                    $constantsFile = $sitepackage->getPackagePath() . 'Configuration/TypoScript/constants.typoscript';
                    $setupFile = $sitepackage->getPackagePath() . 'Configuration/TypoScript/setup.typoscript';

                    if (!file_exists($constantsFile)) {
                        $constantsFile = $sitepackage->getPackagePath() . 'Configuration/TypoScript/constants.txt';
                    }
                    if (!file_exists($setupFile)) {
                        $setupFile = $sitepackage->getPackagePath() . 'Configuration/TypoScript/setup.txt';
                    }

                    if (file_exists($constantsFile)) {
                        $constants = (string)@file_get_contents($constantsFile);
                        // Enthält die constants vom sitepackage
                    } else {
                        $constants = '';
                    }
                    if (file_exists($setupFile)) {
                        $setup = (string)@file_get_contents($setupFile);
                        // Enthält die setup vom sitepackage
                    } else {
                        $setup = '';
                    }

                    // pre-process the lines of the constants and setup and check for "@" syntax
                    // @import
                    // @importTheme
                    // @clear
                    // are the currently allowed syntax (must be on the head of each line)
                    if ($themePackage !== null) {
                        $themeConstantsFile = "@import 'EXT:" . $themePackage . "/Configuration/TypoScript/constants.typoscript'\r\n";
                        $themeSetupFile = "@import 'EXT:" . $themePackage . "/Configuration/TypoScript/setup.typoscript'\r\n";
                        if (strpos($constants, '@importTheme') !== false) {
                            $constants = preg_replace('/importTheme\s?=\s?1/', $themeConstantsFile, $constants);
                        } else {
                            $constants = $themeConstantsFile . "\r\n" . $constants;
                        }
                        if (strpos($setup, '@importTheme') !== false) {
                            $setup = preg_replace('/importTheme\s?=\s?1/', $themeSetupFile, $setup);
                        } else {
                            $setup = $themeSetupFile . "\r\n" . $setup;
                        }
                    }
                    foreach ($pageWebsiteConstantsFile as $importConstants) {
                        $constants .= $importConstants;
                    }

                    $hasRootTemplate = (bool)$templateService->getRootId();
                    /**
                     * @phpstan-ignore-next-line
                     */
                    $fakeRow = [
                        'config' => $setup,
                        'constants' => $constants,
                        'nextLevel' => 0,
                        'static_file_mode' => 1,
                        'tstamp' => $setup ? filemtime($setupFile) : time(),
                        'uid' => 'sys_bolt_' . (int)$pageRecord['uid'] . $sitepackage->getPackageKey(),
                        'title' => $sitepackage->getPackageKey(),
                        // make this the root template
                        'root' => !$hasRootTemplate
                    ];

                    // Enthält neues TS Template
                    $templateService->processTemplate(
                        $fakeRow,
                        'sys_bolt_' . $sitepackage->getPackageKey(),
                        (int)$pageRecord['uid'],
                        'sys_bolt_' . $sitepackage->getPackageKey()
                    );
                    if (!$hasRootTemplate) {
                        // $templateService->processTemplate() adds the constants and setup info
                        // to the very end however, we like to add ours add root template
                        array_pop($templateService->constants);
                        array_unshift($templateService->constants, $constants);
                        array_pop($templateService->config);
                        array_unshift($templateService->config, $setup);
                        // when having the 'root' flag, set $processTemplate resets the rootline -> we don't want that.
                        $hookParameters['rootLine'] = $rootLine;
                        //Hier ist das neue TS-Template fertig
                    }
                }
            }
        }
    }
}
