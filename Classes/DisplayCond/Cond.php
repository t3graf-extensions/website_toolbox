<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\WebsiteToolbox\DisplayCond;

use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Cond
{
    /**
     * Checks for already existing mm relation of tx_myext_object
     * Returns true, if no mm relation found
     *
     * @param  array $configuration
     * @return bool
     */
    public function isThemeActive(array $configuration): bool
    {
        $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
        $root = $siteFinder->getSiteByPageId($configuration['record']['pid']);
        $theme = $root->getAttribute('activeTheme');

        return $theme === $configuration['conditionParameters']['0'];
    }
}
