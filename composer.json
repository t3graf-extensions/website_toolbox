{
    "name": "t3graf/website-toolbox",
    "description": "Versatile, universal and very flexibly customizable website toolbox. It offers a tool for choosing themes (templates) and allows automatic integration of a project sitepackage. It is modularly expandable. See the documentation for more information.",
    "license": "GPL-3.0-or-later",
    "type": "typo3-cms-extension",
    "authors": [
        {
            "name": "T3graf Development-Team",
			"email": "development@t3graf-media.de",
			"homepage": "https://www.t3graf-media.de/",
            "role": "Developer"
        }
    ],
    "require": {
        "php": ">=8.0",
        "spooner-web/just-maintain": "^1.2.0",
        "typo3/cms-core": "^11.5",
        "typo3/cms-t3editor": "^11.5"
    },
    "require-dev": {
        "bk2k/extension-helper": "^2.0",
        "ergebnis/composer-normalize": "*",
        "friendsofphp/php-cs-fixer": "^3.0.0",
        "friendsoftypo3/phpstan-typo3": "^0.9",
        "helhum/typo3-console": "^7.1",
        "helmich/typo3-typoscript-lint": "^v2.3",
        "overtrue/phplint": "^3.2 || ^4.3 || ^5.3",
        "phpstan/phpstan": "^1.7",
        "phpstan/phpstan-deprecation-rules": "^1.0",
        "phpstan/phpstan-phpunit": "^1.1",
        "phpstan/phpstan-strict-rules": "^1.2",
        "roave/security-advisories": "dev-master",
		"t3-themes/t3-theme-diag": "dev-main",
        "t3graf/site-testpage": "*",
        "typo3-console/composer-auto-commands": "^1.0",
        "typo3/cms-backend": "*",
        "typo3/cms-composer-installers": "^4.0",
        "typo3/cms-extbase": "*",
        "typo3/cms-extensionmanager": "*",
        "typo3/cms-felogin": "*",
        "typo3/cms-filelist": "*",
        "typo3/cms-fluid": "*",
        "typo3/cms-form": "*",
        "typo3/cms-frontend": "*",
        "typo3/cms-indexed-search": "*",
        "typo3/cms-info": "*",
        "typo3/cms-install": "*",
        "typo3/cms-lowlevel": "*",
        "typo3/cms-recycler": "*",
        "typo3/cms-rte-ckeditor": "*",
        "typo3/cms-seo": "*",
        "typo3/cms-tstemplate": "*",
        "typo3/testing-framework": "^6.4"
    },
    "replace": {
        "typo3-ter/website-toolbox": "self.version"
    },
    "repositories": {
        "extensions": {
            "type": "path",
            "url": "./Extensions/*",
            "options": {
                "symlink": false
            }
        }
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "autoload": {
        "psr-4": {
            "T3graf\\WebsiteToolbox\\": "Classes"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "T3graf\\WebsiteToolbox\\Tests\\": "Tests"
        }
    },
    "config": {
        "allow-plugins": {
            "typo3/cms-composer-installers": true,
            "typo3/class-alias-loader": true,
            "ergebnis/composer-normalize": true,
            "phpstan/extension-installer": true
        },
        "bin-dir": ".Build/bin",
        "optimize-autoloader": true,
        "sort-packages": true,
        "vendor-dir": ".Build/vendor"
    },
    "extra": {
        "branch-alias": {
            "dev-master": "2.0.x-dev"
        },
        "typo3/cms": {
            "cms-package-dir": "{$vendor-dir}/typo3/cms",
            "extension-key": "website_toolbox",
            "web-dir": ".Build/public"
        }
    },
    "scripts": {
        "post-autoload-dump": [
            "@link-extension"
        ],
        "changelog": [
            "extension-helper changelog:create"
        ],
        "ci": [
            "@ci:static"
        ],
        "ci:composer:normalize": "@composer normalize --dry-run",
        "ci:dynamic": [
            "@ci:tests"
        ],
        "ci:json:lint": "find . ! -path '*.Build/*' ! -path '*node_modules/*' -name '*.json' | xargs -r php .Build//jsonlint -q",
        "ci:php": [
            "@ci:php:copypaste",
            "@ci:php:cs-fixer",
            "@ci:php:lint",
            "@ci:php:sniff"
        ],
        "ci:php:copypaste": "@php .gitlab/tools/phpcpd Classes",
        "ci:php:cs-fixer": "php-cs-fixer fix --config .gitlab/.php-cs-fixer.dist.php -v --dry-run --using-cache no --diff",
        "ci:php:lint": "find .*.php *.php Classes Configuration Tests -name '*.php' -print0 | xargs -r -0 -n 1 -P 4 php -l",
        "ci:php:sniff": "phpcs Classes Configuration Tests",
        "ci:php:stan": "phpstan analyze --configuration .gitlab/phpstan.neon --no-progress",
        "ci:static": [
            "@ci:composer:normalize",
            "@ci:json:lint",
            "@ci:php:copypaste",
            "@ci:php:cs-fixer",
            "@ci:php:lint",
            "@ci:ts:lint",
            "@ci:yaml:lint"
        ],
        "ci:tests": [
            "@ci:tests:unit",
            "@ci:tests:functional"
        ],
        "ci:tests:functional": "find 'Tests/Functional' -wholename '*Test.php' | parallel --gnu 'echo; echo \"Running functional test suite {}\"; .Build//phpunit -c .Build/vendor/typo3/testing-framework/Resources/Core/Build/FunctionalTests.xml {}';",
        "ci:tests:unit": ".Build//phpunit -c .Build/vendor/typo3/testing-framework/Resources/Core/Build/UnitTests.xml Tests/Unit",
        "ci:ts:lint": "typoscript-lint -c gitlab/typoscript-lint.yml --ansi -n --fail-on-warnings -vvv Configuration/TypoScript",
        "ci:yaml:lint": "find . ! -path '*.Build/*' ! -path '*node_modules/*' -regextype egrep -regex '.*.ya?ml$' | xargs -r php ./.Build/bin/yaml-lint",
        "fix:php": [
            "@fix:php:cs",
            "@fix:php:sniff"
        ],
        "fix:php:cs": "php-cs-fixer fix --config .gitlab/.php-cs-fixer.dist.php",
        "fix:php:sniff": "phpcbf Classes Configuration Tests -w",
		"fix:composer:normalize": "@composer normalize",
        "link-extension": [
            "mkdir -p .Build/vendor/t3graf/",
            "[ -L .Build/vendor/t3graf/website_toolbox ] || ln -snvf ../../../../. .Build/vendor/t3graf/website_toolbox"
        ],
        "phpstan:baseline": "phpstan analyze --configuration .gitlab/phpstan.neon --generate-baseline .gitlab/phpstan-baseline.neon",
        "release:create": [
            "extension-helper release:create"
        ],
        "typo3-cms-scripts": [
            "typo3cms install:fixfolderstructure"
        ]
    },
    "scripts-descriptions": {
        "ci": "Runs all dynamic and static code checks.",
        "ci:composer:normalize": "Checks the composer.json.",
        "ci:dynamic": "Runs all PHPUnit tests (unit and functional).",
        "ci:json:lint": "Lints the JSON files.",
        "ci:php": "Runs all static checks for the PHP files.",
        "ci:php:copypaste": "Checks for copy'n'pasted PHP code.",
        "ci:php:cs-fixer": "Checks the code style with the PHP Coding Standards Fixer (PHP-CS-Fixer).",
        "ci:php:lint": "Lints the PHP files for syntax errors.",
        "ci:php:sniff": "Checks the code style with PHP_CodeSniffer (PHPCS).",
        "ci:php:stan": "Checks the PHP types using PHPStan.",
        "ci:static": "Runs all static code checks (syntax, style, types).",
        "ci:tests": "Runs all PHPUnit tests (unit and functional).",
        "ci:tests:functional": "Runs the functional tests.",
        "ci:tests:unit": "Runs the unit tests.",
        "ci:ts:lint": "Lints the TypoScript files.",
        "ci:yaml:lint": "Lints the YAML files.",
        "fix:php": "Runs all fixers for the PHP code.",
        "fix:php:cs": "Fixes the code style with PHP-CS-Fixer.",
        "fix:php:sniff": "Fixes the code style with PHP_CodeSniffer.",
        "phpstan:baseline": "Updates the PHPStan baseline file to match the code."
    }
}
