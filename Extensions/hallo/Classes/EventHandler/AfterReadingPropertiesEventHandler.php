<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\Hallo\EventHandler;

use T3graf\Hallo\EventHandler\ReadService\FontService;
use T3graf\Hallo\EventHandler\ReadService\SocialNetworkService;
use T3graf\WebsiteToolbox\Event\AfterReadingPropertiesEvent;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AfterReadingPropertiesEventHandler
{
    /** @var array */
    protected $formFields = [];

    public function __invoke(AfterReadingPropertiesEvent $event): void
    {
        $formFields = $event->getFormFields();
        $classes = [FontService::class//, SocialNetworkService::class
        ];
        foreach ($classes as $class) {
            $formFields = GeneralUtility::makeInstance($class, $formFields)->process();
        }
        $event->setFormFields($formFields);
    }
}
