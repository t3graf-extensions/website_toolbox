<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\Hallo\EventHandler\PersistService;

use T3graf\WebsiteToolbox\Utility\TcaUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FontService extends AbstractService
{
    /** @var string */
    protected $googleFont;
    /** @var string */
    protected $googleFontWeight;
    /** @var string */
    protected $googleHeadingsFont;
    /** @var string */
    protected $googleHeadingsFontWeight;

    /*
    public function process(): void
    {
        $this->handleGoogleFont()->handleDifferentHeadingsFont();
    }

    public function getGoogleFont(): string
    {
        return $this->googleFont ?? $this->getUrlSegmentFromField('google_font');
    }

    public function getGoogleFontWeight(): string
    {
        return $this->googleFontWeight ?? $this->getUrlSegmentFromField('google_font_weight');
    }

    public function getGoogleHeadingsFont(): string
    {
        return $this->googleHeadingsFont ?? $this->getUrlSegmentFromField('headings_font');
    }

    public function getGoogleHeadingsFontWeight(): string
    {
        return $this->googleHeadingsFontWeight ?? $this->getUrlSegmentFromField('headings_font_weight');
    }

    protected function handleDifferentHeadingsFont(): self
    {
        if (!(bool)$this->getPropertyValueByFieldName('headings_font_enable')) {
            $this->typoScriptMapper->removePropertyFromBoth(
                TcaUtility::getMappingPath('headings_font')
            );
            $this->typoScriptMapper->removePropertyFromBoth(
                TcaUtility::getMappingPath('headings_font_weight')
            );
            $this->typoScriptMapper->removePropertyFromBoth(
                TcaUtility::getMappingPath('headings_font_family')
            );
        }
        return $this;
    }

    protected function handleGoogleFont(): self
    {
        if (
            (bool)$this->getPropertyValueByFieldName('google_font_enable') &&
            $this->getGoogleFont() !== ''
        ) {
            $this->enableGoogleFont();
            return $this;
        }
        $this->disableGoogleFont();
        return $this;
    }

    protected function enableGoogleFont(): void
    {
        $this->enableGoogleNormalFont();
        if (
            (bool)$this->getPropertyValueByFieldName('headings_font_enable') &&
            $this->getGoogleHeadingsFont() !== ''
        ) {
            $this->enableGoogleHeadingsFont();
        } else {
            $this->disableGoogleHeadingsFont();
        }
        $this->updateGoogleFontUrl();
    }

    protected function disableGoogleFont(): void
    {
        $field = 'font_family';
        $this->typoScriptMapper->bufferProperty(TcaUtility::getMappingPath($field), $this->excludeItemFromList(
            $this->getPropertyValueByFieldName($field),
            '"#{$google-webfont}"'
        ));
        $this->disableGoogleHeadingsFont();
        $this->typoScriptMapper->removePropertyFromBoth('page.theme.hallo.googleFontsUrl');
    }

    protected function disableGoogleHeadingsFont(): void
    {
        $field = 'headings_font_family';
        $this->typoScriptMapper->bufferProperty(TcaUtility::getMappingPath($field), $this->excludeItemFromList(
            $this->getPropertyValueByFieldName($field),
            '"#{$google-headings-webfont}"'
        ));
    }

    protected function enableGoogleNormalFont(): self
    {
        if ($this->getGoogleFontWeight() === '') {
            $this->googleFontWeight = '400';
            $this->typoScriptMapper->bufferProperty(
                TcaUtility::getMappingPath('google_font_weight'),
                $this->getGoogleFontWeight()
            );
        }
        $this
            ->addGoogleFontToFontFamily(
                'font_family',
                '"#{$google-webfont}"'
            )
            ->addGoogleFontWeightToFontWeight(
                'font_weight',
                $this->getGoogleFontWeight()
            );
        return $this;
    }

    protected function enableGoogleHeadingsFont(): self
    {
        if ($this->getGoogleHeadingsFontWeight() === '') {
            $this->googleHeadingsFontWeight = '400';
            $this->typoScriptMapper->bufferProperty(
                TcaUtility::getMappingPath('headings_font_weight'),
                $this->getGoogleHeadingsFontWeight()
            );
        }
        $this
            ->addGoogleFontToFontFamily(
                'headings_font_family',
                '"#{$google-headings-webfont}"'
            )
            ->addGoogleFontWeightToFontWeight(
                'headings_font_weight',
                $this->getGoogleHeadingsFontWeight()
            );
        return $this;
    }

    protected function updateGoogleFontUrl(): self
    {
        if (
            !(bool)$this->getPropertyValueByFieldName('headings_font_enable') ||
            $this->getGoogleFont() === '' ||
            $this->getGoogleFontWeight() === '' ||
            $this->getGoogleHeadingsFont() === '' ||
            $this->getGoogleHeadingsFontWeight() === '' ||
            $this->getGoogleFont() === $this->getGoogleHeadingsFont()
        ) {
            $this->typoScriptMapper->removePropertyFromBoth('page.theme.hallo.googleFontsUrl');
            return $this;
        }
        $this->typoScriptMapper->bufferProperty('page.theme.hallo.googleFontsUrl', sprintf(
            'https://fonts.googleapis.com/css?display=swap&family=%s:%s|%s:%s',
            $this->getGoogleFont(),
            $this->getGoogleFontWeight(),
            $this->getGoogleHeadingsFont(),
            $this->getGoogleHeadingsFontWeight(),
        ));

        return $this;
    }

    protected function addGoogleFontToFontFamily(string $field, string $scssFontInterpolation): self
    {
        $this->typoScriptMapper->bufferProperty(TcaUtility::getMappingPath($field), $this->includeItemInList(
            $this->getPropertyValueByFieldName($field),
            $scssFontInterpolation
        ));
        return $this;
    }

    protected function addGoogleFontWeightToFontWeight(string $field, string $fontWeights): self
    {
        $suggestedFontWeight = $suggestedFontWeight ?? '';
        if (
            ($currentFontWeight = trim($this->getPropertyValueByFieldName($field))) === '' ||
            strpos($fontWeights, $currentFontWeight) === false &&
            ($suggestedFontWeight = (int)(GeneralUtility::trimExplode(',', $fontWeights)[0] ?? 400)) > 0
        ) {
            $this->typoScriptMapper->bufferProperty(TcaUtility::getMappingPath($field), (string)$suggestedFontWeight);
        }
        return $this;
    }

    protected function includeItemInList(string $list, string $item): string
    {
        if (strpos($list, $item) !== false) {
            return $list;
        }
        if (trim($list) === '') {
            $list = 'sans-serif';
        }
        $items = array_merge([$item], GeneralUtility::trimExplode(',', $list, true));
        return implode(', ', $items);
    }

   /* protected function excludeItemFromList(string $list, string $item): string
    {
        if (strpos($list, $item) === false) {
            return $list;
        }
        $list = str_replace($item, '', $list);
        return implode(', ', GeneralUtility::trimExplode(',', $list, true));
    }
*/
    /* protected function getUrlSegmentFromField(string $field): string
     {
         return (string)str_replace(' ', '+', trim($this->getPropertyValueByFieldName($field)));
     }
    */
}
