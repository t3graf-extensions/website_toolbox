<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\Hallo\EventHandler\PersistService;

use T3graf\WebsiteToolbox\Utility\TcaUtility;

class CookieService extends AbstractService
{
    public function process(): void
    {
        if ((bool)$this->getPropertyValueByFieldName('cookie_static')) {
            $this->typoScriptMapper->bufferProperty(
                TcaUtility::getMappingPath('cookie_position'),
                'top'
            );
        }
    }
}
