<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') or die('Access denied.');
// Add default RTE configuration
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['hallo'] = 'EXT:hallo/Configuration/RTE/Default.yaml';

// PageTS
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:hallo/Configuration/TsConfig/Page/All.tsconfig">');
