<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'file' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_normal',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'imagePreview',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.file',
                    ],
                ],
                'fileInverted' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_inverted',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'imagePreview',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.fileInverted',
                    ],
                ],
                'width' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_width',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.width',
                    ],
                ],
                'height' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_height',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.height',
                    ],
                ],
                'alt' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_alt_tag',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.alt',
                    ],
                ],
                'linktitle' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:logo_link_title',
                    'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.logo.linktitle',
                    ],
                ],
                'favicon_path' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:favicon_path',
                    //'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.favicon.path',
                    ],
                ],
                'favicon_file' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:favicon_file',
                    //'description' => 'field description',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.favicon.file',
                    ],
                ],
                'favicon_generator_text' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:favicon_generated_code',
                    //'description' => 'field description',
                    'config' => [
                        'type' => 'text',
                        'renderType' => 't3editor',
                        'format' => 'html',
                        'rows' => 7,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.favicon.file.code',
                    ],
                ],

            ],

            'palettes' => [
                'logo' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:palette_logo',
                    //'description' => 'LL: Header description',
                    'showitem' => 'file, fileInverted, --linebreak--, width, height, --linebreak--, alt, linktitle',
                ],
                'favicon' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:palette_favicon',
                    'description' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf:palette_favicon_description',
                    'showitem' => 'favicon_path, favicon_generator_text, --linebreak--, favicon_file,',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:hallo/Resources/Private/Language/locallang_tab_logo.xlf')
        ->addDiv(
            'LANG:tabLogo',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabLogo', 'favicon')
        ->addPaletteToDiv('LANG:tabLogo', 'logo')
        ->saveToTca(false);
}
