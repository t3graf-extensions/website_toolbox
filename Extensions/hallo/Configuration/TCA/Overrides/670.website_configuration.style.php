<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/*
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'enable-rounded' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf:enable_rounded',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-rounded',
                    ],
                ],
                'enable-shadows' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf:enable_shadows',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-shadows',
                    ],
                ],
                'enable-gradients' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf:enable_gradients',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-gradients',
                    ],
                ],
                'enable_transitions' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf:enable_transitions',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-transitions',
                    ],
                ],
            ],

            'palettes' => [
                'style' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf:palette_style',
                    //'description' => 'LL:description',
                    'showitem' => 'enable-rounded, enable-shadows, enable-gradients, enable_transitions',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:hallo/Resources/Private/Language/locallang_tab_style.xlf')
        ->addPalette('style', 'after:' . $typesBuilder->getPaletteString('configuration'))
        ->saveToTca(false);
}
*/
