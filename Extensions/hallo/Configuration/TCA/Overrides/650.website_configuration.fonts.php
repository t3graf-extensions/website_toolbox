<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // Available Google fonts
    $googleFonts = [
        ['Roboto', 'Roboto'],
        ['Open Sans', 'Open Sans'],
        ['Lato', 'Lato'],
        ['Monserrat', 'Monserrat'],
        ['Roboto Condensed', 'Roboto Condensed'],
        ['Source Sans Pro', 'Source Sans Pro'],
        ['Oswald', 'Oswald'],
        ['Raleway', 'Raleway'],
        ['PT Sans', 'PT Sans'],
        ['Noto Sans', 'Noto Sans'],
        ['Open Sans Condensed', 'Open Sans Condensed'],
        ['Ubuntu', 'Ubuntu'],
        ['Poppins', 'Poppins'],
        ['---', ''],
        ['Slabo 27px', 'Slabo 27px'],
        ['Merriweather', 'Merriweather'],
        ['Roboto Slab', 'Roboto Slab'],
        ['Noto Serif', 'Noto Serif'],
        ['Playfair Display', 'Playfair Display'],
        ['Bitter', 'Bitter'],
        ['Arvo', 'Arvo'],
        ['Libre Baskerville', 'Libre Baskerville'],
        ['---', ''],
        ['Lobster', 'Lobster'],
        ['Indie Flower', 'Indie Flower']
    ];
    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'google_font_enable' => [
                    'label' => 'LL:google_font_enable',
                    'exclude' => 1,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.enable',
                    ],
                ],
                'headings_font_enable' => [
                    'label' => 'LL:headings_font_enable',
                    'exclude' => 1,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFont.enable',
                    ],
                ],
                'google_font' => [
                    'label' => 'LL:google font',
                    'displayCond' => 'FIELD:google_font_enable:REQ:true',
                    'exclude' => 0,
                    'eval' => 'required',
                    'config' => [
                        'type' => 'input',
                        'valuePicker' => [
                            'items' => $googleFonts
                        ],

                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.font',
                    ],
                ],
                'google_font_weight' => [
                    'label' => 'LL:google font weight',
                    'displayCond' => 'FIELD:google_font_enable:REQ:true',
                    'exclude' => 0,
                    'eval' => 'required',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.weight',
                    ],
                ],
                'headings_font' => [
                    'label' => 'LL:headings font',
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:headings_font_enable:REQ:true'
                        ],
                    ],
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                        'valuePicker' => [
                            'items' => $googleFonts
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFont.font',
                    ],
                ],
                'headings_font_weight' => [
                    'label' => 'LL:headings font weight',
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:headings_font_enable:REQ:true'
                        ],
                    ],
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFont.weight',
                    ],
                ],
                'font_family' => [
                    'label' => 'll:font family',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.fontFamily',
                    ],
                ],
                'font_weight' => [
                    'label' => 'll:font weight',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-weight-normal',
                    ],
                ],
                'font_weight_light' => [
                    'label' => 'll:font weight light',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.fontWeightLight',
                    ],
                ],
                'font_weight_bold' => [
                    'label' => 'll:font weight bold',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.fontWeightBold',
                    ],
                ],
                'font_size_base' => [
                    'label' => 'll:font size base',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.fontSizeBase',
                    ],
                ],
                'headings_font_family' => [
                    'label' => 'll:headnings font family',
                    'exclude' => 1,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:headings_font_enable:REQ:true'
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFontFamily',
                    ],
                ],
                'headings_font_style' => [
                'label' => 'LL:headings font style',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFontStyle',
                    ],
            ],
                'headings_line_height' => [
                'label' => 'LL:headings line height',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsLineHeight',
                    ],
            ],
                'headings_h1_font_size' => [
                'label' => 'LL:headings h1 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h1-font-size',
                    ],
            ],
                'headings_h2_font_size' => [
                'label' => 'LL:headings h2 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h2-font-size',
                    ],
            ],
                'headings_h3_font_size' => [
                'label' => 'LL:headings h3 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h3-font-size',
                    ],
            ],
                'headings_h4_font_size' => [
                'label' => 'LL:headings h4 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h4-font-size',
                    ],
            ],
                'headings_h5_font_size' => [
                'label' => 'LL:headings h5 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h5-font-size',
                    ],
            ],
                'headings_h6_font_size' => [
                'label' => 'LL:headings h6 font size',
                'exclude' => 1,
                'config' => [
                    'type' => 'input',
                ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h6-font-size',
                    ],
            ],
],
            'palettes' => [
                'google_font_control' => [
                    'label' => 'LL:google font  control',
                    //'description' => 'LL: Header description',
                    'showitem' => 'google_font_enable, headings_font_enable',
                ],
                'google_font' => [
                    'label' => 'google font',
                    //'description' => 'LL: Header description',
                    'showitem' => 'google_font, google_font_weight, --linebreak--, headings_font, headings_font_weight',
                ],
                'font_normal' => [
                    'label' => 'll:Normal',
                    //'description' => 'LL: Header description',
                    'showitem' => 'font_family, font_weight, --linebreak--, font_weight_light, font_weight_bold, --linebreak--, font_size_base',
                ],
                'font_headings' => [
                    'label' => 'll:Headings',
                    //'description' => 'LL: Header description',
                    'showitem' => 'headings_font_family, headings_font_weight,--linebreak--, headings_font_style, headings_line_height, --linebreak--, headings_h1_font_size, headings_h2_font_size, --linebreak--, headings_h3_font_size, headings_h4_font_size, --linebreak--, headings_h5_font_size, headings_h6_font_size',
                ],
            ],
        ],
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:hallo/Resources/Private/Language/locallang_tab_fonts.xlf')
        ->addDiv(
            'LANG:tabFonts',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabFonts', 'font_headings')
        ->addPaletteToDiv('LANG:tabFonts', 'font_normal')
        ->addPaletteToDiv('LANG:tabFonts', 'google_font')
        ->addPaletteToDiv('LANG:tabFonts', 'google_font_control')
        ->saveToTca(false);
}
