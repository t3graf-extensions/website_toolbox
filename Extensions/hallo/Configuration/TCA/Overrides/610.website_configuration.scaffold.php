<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            //TODO add owner fields
            'columns' => [
                'showTopbar' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:field_showTopbar',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.enabled',
                    ],
                ],
                'showAddress' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_address',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.address',
                    ],
                ],
                'showContact' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_contact',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.contact',
                    ],
                ],
                'showSocialLinks' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_social_links',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.sociallinks',
                    ],
                ],
                'showLanguageMenu' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_language_menu',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.languageMenu',
                    ],
                ],
                'showLogin' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_login',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.loginLink.enable',
                    ],
                ],
                'showFooter' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_footer',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.enable',
                        'valueMap' => [
                            0 => false,  // path.to.myField = false
                            1 => true,   // path.to.myField = true
                        ],
                    ],
                ],
                'footerFullWith' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_full_width',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.fullwidth',
                    ],
                ],
                'footerSlideOut' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_slide_out',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.slideout',
                    ],
                ],
                'footerMetaSection' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_footer_meta_section',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.meta.enable',
                    ],
                ],
                'footerLanguageSection' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:show_language_section',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.language.enable',
                    ],
                ],
            ],
            'palettes' => [
                'topbar' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_topbar',
                    'showitem' => 'showTopbar,',
                ],
                'topbarElements' => [
                    'label' => '',
                    'description' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_topbar_elements_description',
                    'showitem' => 'showAddress, showContact, showSocialLinks, --linebreak--, showLanguageMenu, showLogin,',
                ],
                'footer' => [
                    'label' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer',
                    'showitem' => 'showFooter, footerFullWith, footerSlideOut,',
                ],
                'footerElements' => [
                    'label' => '',
                    'description' => 'LLL:EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer_elements_description',
                    'showitem' => 'footerMetaSection, footerLanguageSection, ',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:hallo/Resources/Private/Language/locallang_tab_scaffold.xlf')
        ->addDiv(
            'LANG:tabScaffold',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        //->addFieldToDiv('LANG:tabLayout', 'agency_slogan')
        ->addPaletteToDiv('LANG:tabScaffold', 'footerElements')
        ->addPaletteToDiv('LANG:tabScaffold', 'footer')
        ->addPaletteToDiv('LANG:tabScaffold', 'topbarElements')
        ->addPaletteToDiv('LANG:tabScaffold', 'topbar')

        ->saveToTca(true);
}
