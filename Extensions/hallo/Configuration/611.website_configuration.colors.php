<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

\TYPO3\CMS\Core\Utility\DebugUtility::debug(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox'), 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
\TYPO3\CMS\Core\Utility\DebugUtility::debug(\T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo'), 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
//die();
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('hallo')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes

    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:hallo/Resources/Private/Language/locallang_db.xml')
        ->addDiv('tabColors', 'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance')
        ->addFieldToDiv('tabColors', 'agency_slogan')
        ->saveToTca(false);
}
