.. include:: Includes.txt
.. _start:

=============================================================
Website Toolbox
=============================================================

:Extension key:
   website_toolbox

:Package name:
   t3graf/website-toolbox

:Version:
   |release|

:Language:
   en

:Authors:
   T3graf Development-Team

:Email:
   development@t3graf-media.de

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

:Rendered:
   |today|

----

Versatile, universal and very flexibly customizable website toolbox. It offers a tool for choosing themes (templates) and allows automatic integration of a project sitepackage. It is modularly expandable. See the documentation for more information.

----

.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Quick start <quickStart>`

         .. container:: card-body

            A quick introduction in how to use this extension.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Introduction <introduction>`

         .. container:: card-body

            Introduction to the extension news, general information.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Editors manual <userManual>`

         .. container:: card-body

            Learn how to use the news administration module, how to configure
            the plugin and how to create news records, tags and categories.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Tutorials <tutorials>`

         .. container:: card-body

            Tutorials and snippets for many frequent use cases.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Administration <admin-manual>`

         .. container:: card-body

            Install or upgrade EXT:news, learn how to migrate from EXT:tt_news
            to news or from RealURL to Routes etc.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Reference <reference>`

         .. container:: card-body

            In-depth reference about certain aspects of this extension:
            TypoScript, TSconfig, ViewHelpers etc

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Support <support>`

         .. container:: card-body

            There are various ways to get support for EXT:news!

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Addons <addons>`

         .. container:: card-body

            Various extensions improve EXT:news with additional features.
.. Table of Contents

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Introduction/Index
   Editor/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ChangeLog/Index
   Sitemap

.. Meta Menu

.. toctree::
   :hidden:

   Sitemap
   genindex
